# pico-apps setup

This setup is meant to work with exactly this repository, so it may not work with the original repository provided by the lecturer.

## Required packages

Firstly, you need to install `cmake` and `gcc-arm-embedded` packages. On MacOS you can use `homebrew` to install these packages.

If you don't have `homebrew` installed, run this command to install it:

```
$ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

To install `cmake` run this command:

```
$ brew install cmake
```

To install `gcc-arm-embedded` run this command:

```
$ brew install --cask gcc-arm-embedded
```

## Required VSCode extensions

- https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools

## Next steps

1. Clone this repository.

2. Open the repository folder in VSCode.

3. Press `Ctrl+Shift+P` or `Cmd+Shift+P` and type `CMake: Select a Kit`, then press `Enter`.

4. In the appeared pop-up menu select the option which contains `arm-none-aebi`.\
   If this kit doesn't appear in the list, try to select `[Scan for kits]` option. Feel free to restart VSCode in this case.

5. Press `Ctrl+Shift+P` or `Cmd+Shift+P` and type `CMake: Build`, then press `Enter`.\
   **(!)** If the `build` folder appeared before you got to this step, you should delete it and restart VSCode, as it contains cache from previous build and will break the following one. This applies only to the first build, in the future you do not need to delete this folder.
